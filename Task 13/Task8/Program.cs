﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task8
{
    class Program
    {
        //Mini shop - the sum of  3 doubles by user input and add GST (15%) 
        //Output must show $ signs

        static void Main(string[] args)
        {

            double a = 0.0;
            double b = 0.0;
            double c = 0.0;
            double d = 0.0;
            double e = 0.0;

            Console.WriteLine("Please enter your three numbers");
            Console.Write("$"); a = double.Parse(Console.ReadLine());
            Console.Write("$"); b = double.Parse(Console.ReadLine());
            Console.Write("$"); c = double.Parse(Console.ReadLine());

            d = a + b + c;
            e = d / 100 * 15 + d;
            Console.WriteLine($"The amount comes to ${e}");

        }
    }
}
