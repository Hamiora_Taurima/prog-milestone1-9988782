﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {

            //Get user input for the month and day they are born on and display it in a sentence


            //Initialising variables to use for the application
            var Month = "";
            var Day = 0;

            //Instructions + User input
            Console.WriteLine("Please enter the month you were born");
            Month = Console.ReadLine();
            Console.WriteLine("Please enter the day you were born");
            Day = int.Parse(Console.ReadLine());

            //Displayed information of user input

            Console.WriteLine($"The month you were born was { Month} and the day you were born was the {Day}" );
        }
    }
}
