﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task20
{
    class Program
    {

        //


        static void Main(string[] args)
        {
            var counter = 20;
            var i = 0;

            for (i = 0; i < counter; i++)
            {
                var a = i + 1;

                if ((a % 2 == 0))
                {
                    Console.WriteLine($"{a} this is an even number.");
                }
                else
                {
                    Console.WriteLine($"{a} this is an odd number");
                }

            }
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("You have reached the end of your counter goodbye");
            Console.ReadLine();


        }
    }
}
