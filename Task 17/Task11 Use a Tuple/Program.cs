﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11_Use_a_Tuple
{
    class Program
    {

        //Task requires the use of a tuple

        static void Main(string[] args)
        {
            var Nameandage = new List<Tuple<string, int>>();

            Nameandage.Add(Tuple.Create("Hami", 13));
            Nameandage.Add(Tuple.Create("Pip", 27));
            Nameandage.Add(Tuple.Create("Mahia", 11));

            foreach (var x in Nameandage)
            {
                Console.WriteLine($"This is {x.Item1} they are {x.Item2}.");
            }
            Console.ReadLine();

        }
    }
}
