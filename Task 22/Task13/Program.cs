﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task13
{
    class Program
    {

        //Create a program that takes user input of 3 words.
        //Split the string up into an array
        //Print the array to the screen with one word on each line

        static void Main(string[] args)
        {

            var dictionary = new Dictionary<string, string>();
            var fruitsList = new List<string>();
            var VegetablesList = new List<string>();

            dictionary.Add("Kumara", "Vegetable");
            dictionary.Add("Feijoa", "Fruit");
            dictionary.Add("Peach", "Fruit");
            dictionary.Add("Plum", "Fruit");
            dictionary.Add("Kiwifruit", "Fruit");
            dictionary.Add("Marrow", "Vegetable");
            dictionary.Add("Cauliflower", "Vegetable");
            dictionary.Add("Brusselsprout", "Vegetable");


            foreach (var x in dictionary)
            {
                if (x.Value == "Fruit")
                {
                    fruitsList.Add(x.Key);
                }
                if (x.Value == "Vegetable")
                {
                    VegetablesList.Add(x.Key);
                }
            }
            Console.WriteLine($"There are {fruitsList.Count} Fruits in this list.");
            Console.WriteLine($"There are {VegetablesList.Count} Vegetables in this list.");
            Console.WriteLine($"The Fruits are {string.Join("-", fruitsList)}");

            Console.ReadLine();
        }
    }
}
