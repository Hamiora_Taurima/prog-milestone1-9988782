﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    class Program
    {

        //Create a program that takes user input of 3 words.
        //Split the string up into an array
        //Print the array to the screen with one word on each line


        static void Main(string[] args)
        {

            Console.WriteLine("  ");
            Console.WriteLine("Please enter three words followed by Pressing Enter.");

            var userInput = Console.ReadLine();

            var output = userInput.Split(' ');
            Console.WriteLine($"{output[0]}");
            Console.WriteLine($"{output[1]}");
            Console.WriteLine($"{output[2]}");
        }



    }
    }

