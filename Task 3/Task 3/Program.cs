﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {

        //Using if statements and / or switches convert KM to Miles and MIles to KM


        static void Main(string[] args)
        {



            Console.WriteLine("Please select one of the options ");
            Console.WriteLine("1 to convert KM to Miles");
            Console.WriteLine("2 to convert Miles to KM");


            var menuselection = Console.ReadLine();
            Console.Clear();
            
            switch (menuselection)
            {

                case "1":

                    var distance = 0;
                    var answer = 0;

                    Console.WriteLine("Please enter the distance you want to convert");
                    distance = int.Parse(Console.ReadLine());
                    answer = (int)Math.Round(distance * 0.62);

                    Console.WriteLine($"That is {answer} Miles");

                    break;

                case "2":
                   
                    var distance2 = 0;
                    var answer2 = 0;

                    Console.WriteLine("Please enter the distance you want to convert");
                    distance2 = int.Parse(Console.ReadLine());
                    answer2 = (int)Math.Round(distance2 / 0.62);

                    Console.WriteLine($"That is {answer2} KM");
                    break;


                default:
                    Console.WriteLine("The selection was invalid");
                    break;


                  

            }





        }
    }
}
