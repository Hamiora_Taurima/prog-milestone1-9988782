﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {

            // print a countdown from 99 to 0 using no more than 5 lines of code in the loop block

            var counter = 99;
            var i = 0;

            for (i = -1; i < counter; counter--)
            {
                

                Console.WriteLine($"{counter}");


            }

        }
    }
}
