﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {


            //Get user input for name and age and show 3 different ways to print a string


            //Initialising variables
            var name = "";
            var age = 0;

            //User inputs name and age
            Console.WriteLine("Please enter your name");
            name = Console.ReadLine();
            Console.WriteLine("Please enter your age");
            age = int.Parse(Console.ReadLine());

            //Application displays entered information
            Console.WriteLine("Your name is " + name + " and you are " + age + " years old ");
            Console.WriteLine("Your name is {0} and you are {1} years old", name, age);
            Console.WriteLine($"Your name is {name} and you are {age} years old");

        }
    }
}
