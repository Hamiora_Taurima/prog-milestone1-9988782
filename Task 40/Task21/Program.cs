﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task21
{
    class Program
    {

        //Assuming that the 1st of the month is on a Monday (like this month) write a program that tells you how many wednesdays are in a month.
        static void Main(string[] args)
        {

            int entry;

            var weeks = new string[] {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday" };


        Top:


           int wednesdays = 0;
            int counter = 0;

            Console.WriteLine("Please enter the total days this month 28,29,30,31.");

            if (int.TryParse(Console.ReadLine(), out entry))

            {

                for (int count = 0; count < entry; count++)
                {
                    counter++;
                    Console.WriteLine($"Day {count + 1} is a {weeks[counter - 1]}.");

                    if (counter == 3)
                    {
                        wednesdays++;
                    }
                    else if (counter == 7)
                    {
                        counter = 0;
                    }
                }
                Console.WriteLine($"Your entry of {entry} shows there will be {wednesdays} wednesdays this month if the 1st is a monday");


                goto Top;
            }

        }
    }
}
