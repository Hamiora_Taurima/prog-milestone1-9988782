﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4_celsius_to_fahrenheit
{
    class Program
    {

        //Using if statements and / or switches convert Celsius to Fahrenheit and Fahrenheit to Celsius


        static void Main(string[] args)
        {


            Console.WriteLine("Please select one of the options ");
            Console.WriteLine("1 to convert Celcius to Fahrenheit");
            Console.WriteLine("2 to convert Fahrenheit to Celcius");


            var menuselection = Console.ReadLine();
            Console.Clear();

            switch (menuselection)
            {

                case "1":

                    var Temperature = 0;
                    double answer = 0;

                    Console.WriteLine("Please enter the temperature you want to convert");
                    Temperature = int.Parse(Console.ReadLine());
                    answer = Temperature * 9 / 5 + 32;

                    Console.WriteLine($"That is {answer} degrees fahrenheit");

                    break;

                case "2":

                    var Temperature2 = 0;
                    double answer2 = 0;

                    Console.WriteLine("Please enter the temperature you want to convert");
                    Temperature2 = int.Parse(Console.ReadLine());
                    answer2 = (Temperature2 - 32) * 5 / 9;

                    Console.WriteLine($"That is {answer2} degrees Celcius");
                    break;


                default:
                    Console.WriteLine("The selection was invalid");
                    break;



            }
        }
    }
}
