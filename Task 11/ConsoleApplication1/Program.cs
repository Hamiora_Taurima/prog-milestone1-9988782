﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6

{
    class Program
    {
        static void Main(string[] args)
        {

            //Checks if a year was a leap year (yes or no answer to the user)

            int a = 0;

            Console.WriteLine("Enter the date you want to check");
            a = int.Parse(Console.ReadLine());

            if ((a % 4 == 0 && a % 100 != 0) || (a % 400 == 0))
            {
                Console.WriteLine($"Yes");
            }

            else
            { 
                Console.WriteLine($"No");
            }




        }
    }
}