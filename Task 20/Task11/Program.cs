﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11
{
    class Program
    {

        //You are presented with an array of numbers
        //The array for you to use has the following values(33, 45, 21, 44, 67, 87, 86)
        //Put in all the odd numbers in a new list
        //Print the List

        static void Main(string[] args)
        {

            var val = new int[7] { 33, 45, 21, 44, 67, 87, 86 };
            List<int> even = new List<int>();
            List<int> odd = new List<int>();
            foreach (int number in val)
                if (number % 2 == 0)
                {
                    even.Add(number);
                }
                else
                {
                    odd.Add(number);
                }
            
            Console.WriteLine($"These are the odd numbers {string.Join(",", odd)}");
        }
            }
        }
                
        
    
