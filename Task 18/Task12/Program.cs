﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task12
{
    class Program
    {


        //Create a List<T> that holds people’s name and month and birthday.


        static void Main(string[] args)
        {
            var Entry = new List<Tuple<string, int, string>>();

            Entry.Add(Tuple.Create("Hami", 13, "Novemer"));
            Entry.Add(Tuple.Create("Pip", 27, "September"));
            Entry.Add(Tuple.Create("Mahia", 11, "March"));

            foreach (var x in Entry)
            {
                Console.WriteLine($"{x.Item1} was born in New Zealand on {x.Item2}th of {x.Item3}.");
            }
            Console.ReadLine();



        }
    }
}
