﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    class Program
    {

                        //Count the number of words in a string


        static void Main(string[] args)
        {

            var Words = "The quick brown fox jumped over the fence";
            var count = 0;
            var seperate = Words.Split(' ');

            foreach (var word in seperate)
            {
                count = count + 1;
            }
            
            Console.WriteLine($" There are {count} words in '{Words}'");
        }
    }
}
