﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        //You are presented with an array of numbers
        //The array for you to use has the following values(34, 45, 21, 44, 67, 88, 86)
        //Put in all the even numbers in a new list
        //Print the List


        static void Main(string[] args)
        {
            var value = new int[7] { 34, 45, 21, 44, 67, 87, 86 };


            List<int> EvenNumber = new List<int>();
            List<int> OddNumber = new List<int>();


            foreach (int number in value)
                if (number % 2 == 0)
                {
                    EvenNumber.Add(number);
                }
                else
                {
                    OddNumber.Add(number);
                }
            Console.WriteLine($"These are the even numbers {string.Join(", ", EvenNumber)}");
            Console.WriteLine($"These are the odd numbers {string.Join(",", OddNumber)}");

        }
    }
}
