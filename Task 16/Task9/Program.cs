﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{
    class Program
    {
        //Count the letters in a word (Display the string and the number of characters it is)


        static void Main(string[] args)
        {
            string a;
            var counter = 0;
         

            Console.WriteLine("Please enter a word");
            a = Console.ReadLine();

            foreach (char letter in a)
            {
               counter++;
            }
            Console.WriteLine($"The word {a} has {counter} letters");
            


        }
    }
}
