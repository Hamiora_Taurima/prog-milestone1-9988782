﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task15
{
    class Program
    {


        //Task requires checking the values of a Key and print it if it is true


        static void Main(string[] args)
        {

            var dictionary = new Dictionary<string, string>();
            var WeekdayList = new List<string>();
            var WeekendList = new List<string>();

            dictionary.Add("Monday", "Weekday");
            dictionary.Add("Tuesday", "Weekday");
            dictionary.Add("Wednesday", "Weekday");
            dictionary.Add("Thursday", "Weekday");
            dictionary.Add("Friday", "Weekday");
            dictionary.Add("Saturday", "Weekend");
            dictionary.Add("Sunday", "Weekend");

            foreach (var x in dictionary)
            {
                if (x.Value == "Weekday")
                {
                   WeekdayList.Add(x.Key);
                }
                if (x.Value == "Weekend")
                {
                 WeekendList.Add(x.Key);
                }
            }
            Console.WriteLine($"There are {WeekdayList.Count} weekdays in this list.");
            Console.WriteLine($"There are {WeekendList.Count} weekends in this list.");
            Console.WriteLine($"The Weekdays are {string.Join("-", WeekdayList)}");
            Console.WriteLine($"The Weekend days are {string.Join("-", WeekendList)}");
            Console.ReadLine();
        }
    }
}
